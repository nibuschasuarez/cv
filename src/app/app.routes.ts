import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { DatosComponent } from "./components/datos/datos.component";
import { ContactoComponent } from "./components/contacto/contacto.component";
import { AgendaComponent } from "./components/agenda/agenda.component";
import { AgregarAgendaComponent } from "./components/agenda/agregar-agenda/agregar-agenda.component";
import { VerAgendaComponent } from "./components/agenda/ver-agenda/ver-agenda.component";
import { InformacionComponent } from "./components/informacion/informacion.component";



const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'datos', component: DatosComponent },
  { path: 'contacto', component: ContactoComponent },
  {path:'agenda',component:AgendaComponent},
  {path:'informacion', component: InformacionComponent},
  {path:'agregar-agenda/:id',component:AgregarAgendaComponent},
  {path:'ver-cita/:id',component:VerAgendaComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' },

];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);